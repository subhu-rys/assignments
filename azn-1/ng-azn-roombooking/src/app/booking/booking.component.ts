import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { Room } from "../modal/room";
import { RoomService } from "../service/room.service";

@Component({
  selector: "app-room",
  template: `
    <nav>
      Welecome,
      <a routerLink="/dashboard" queryParamsHandling="preserve" routerLinkActive="active">Dashboard</a>
      <a routerLink="/booking" queryParamsHandling="preserve" routerLinkActive="active">Booking</a>
      <a routerLink="/logout" routerLinkActive="active">Logout</a>
    </nav>

    <br />

    <h2>Book a Room</h2>
    <ul class="rooms">
      <li *ngFor="let room of (rooms$ | async)">
        <a [routerLink]="[room.id]" queryParamsHandling="preserve"
          ><span class="badge">{{ room.id }}</span
          ><b>Room:</b> {{ room.name | titlecase }}
        </a>
      </li>
    </ul>
    <router-outlet></router-outlet>
  `,
  styles: [
    `
      .rooms {
        margin: 0 0 2em 0;
        list-style-type: none;
        padding: 0;
        width: 60em;
      }
      .rooms li {
        position: relative;
        cursor: pointer;
        background-color: #eee;
        margin: 0.5em;
        padding: 0.3em 0;
        height: 2em;
        border-radius: 4px;
      }

      .rooms li:hover {
        color: #607d8b;
        background-color: #ddd;
        left: 0.1em;
      }

      .rooms a {
        color: #888;
        text-decoration: none;
        position: relative;
        display: block;
      }

      .rooms a:hover {
        color: #607d8b;
      }

      .rooms .badge {
        display: inline-block;
        font-size: small;
        color: white;
        padding: 0.8em 0.7em 0 0.7em;
        background-color: #607d8b;
        line-height: 1em;
        position: relative;
        left: -1px;
        top: -4px;
        height: 2.1em;
        min-width: 16px;
        text-align: right;
        margin-right: 0.8em;
        border-radius: 4px 0 0 4px;
      }
    `
  ]
})
export class BookingComponent implements OnInit {
  rooms$: Observable<Room[]>;

  constructor(
    private roomService: RoomService
  ) {}

  ngOnInit() {
    this.rooms$ = this.roomService.fetchRooms();
  }
}
