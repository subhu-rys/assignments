import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { EMPTY, Observable, of } from "rxjs";
import { mergeMap, take } from "rxjs/operators";
import { Room } from "../modal/room";
import { RoomService } from "./room.service";

@Injectable({
  providedIn: "root"
})
export class RoomBookingResolverService implements Resolve<Room> {
  constructor(private rs: RoomService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Room> | Observable<never> {
    let id = route.paramMap.get("id");
    return this.rs.getRoomInfo(id).pipe(
      take(1),
      mergeMap(room => {
        if (room) {
          return of(room);
        } else {
          this.router.navigate(["/booking"]);
          return EMPTY;
        }
      })
    );
  }
}
