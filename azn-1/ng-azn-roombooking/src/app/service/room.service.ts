import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Room } from "../modal/room";
@Injectable({
  providedIn: "root"
})
export class RoomService {
  private roomAPI = "http://localhost:8080/";

  constructor(private http: HttpClient) {}

  public fetchRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(this.roomAPI + "room").pipe(
      tap((rooms: Room[]) => console.log(rooms)),
      catchError(this.handleError)
    );
  }

  public getRoomInfo(id: string): Observable<Room> {
    return this.http.get<Room>(this.roomAPI + "room/" + id).pipe(
      tap((room: Room) => console.log(room)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError(error.error);
  }
}
