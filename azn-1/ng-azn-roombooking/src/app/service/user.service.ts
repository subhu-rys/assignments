import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { User } from "../modal/user";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class UserService {
  private userAPI = "http://localhost:8080/login"; // URL to web api

  constructor(private http: HttpClient) {}

  public verifyUser(user: User): Observable<User> {
    return this.http.post<User>(this.userAPI, user, httpOptions).pipe(
      tap((verifiedUser: User) =>
        console.log(`Verified User id=${verifiedUser.id}`)
      ),
      catchError(this.handleError)
    );
  }

  public verifyUserId(user: User): Observable<User> {
    return this.http.get<User>("http://localhost:8080/user/" + user.id).pipe(
      tap((verifiedUser: User) =>
        console.log(`Verified User id=${verifiedUser.id}`)
      ),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError(error.error);
  }
}
