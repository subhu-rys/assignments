import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { Booking } from "../modal/booking";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class BookingService {
  private bookingAPI = "http://localhost:8080/";

  constructor(private http: HttpClient) {}

  public fetchBooking(): Observable<Booking[]> {
    return this.http.get<Booking[]>(this.bookingAPI + "bookings").pipe(
      tap((bookings: Booking[]) => console.log(bookings)),
      catchError(this.handleError)
    );
  }

  public saveBooking(booking: Booking): Observable<String> {
    return this.http.post<String>(this.bookingAPI + "booking", booking).pipe(
      tap((bookingId: String) => console.log(bookingId)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError("Room Unavailable");
  }
}
