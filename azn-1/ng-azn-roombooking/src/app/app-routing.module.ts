import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BookingComponent } from "./booking/booking.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LoginComponent } from "./login/login.component";
import { RoomBookingHomeComponent } from "./room-booking-home/room-booking-home.component";
import { RoomBookingComponent } from "./room-booking/room-booking.component";
import { RoomBookingResolverService } from "./service/room-booking-resolver.service";

const routes: Routes = [
  {
    path: "booking",
    component: BookingComponent,
    children: [
      {
        path: ":id",
        component: RoomBookingComponent,
        resolve: {
          room: RoomBookingResolverService
        }
      },
      {
        path: "",
        component: RoomBookingHomeComponent
      }
    ]
  },
  { path: "dashboard", component: DashboardComponent },
  { path: "logout", redirectTo: "/login" },
  { path: "login", component: LoginComponent },
  { path: "", redirectTo: "/login", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
