import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-room-booking-home",
  template: `
    <p>
      Select a Room to make a booking!
    </p>
  `,
  styles: []
})
export class RoomBookingHomeComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
