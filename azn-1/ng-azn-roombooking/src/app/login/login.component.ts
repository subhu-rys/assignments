import { Component, OnInit } from "@angular/core";
import { NavigationExtras, Router } from "@angular/router";
import { User } from "../modal/user";
import { UserService } from "../service/user.service";
@Component({
  selector: "app-login",
  template: `
    <h2>Login</h2>
    <form (ngSubmit)="onSubmit()" #loginForm="ngForm">
      <div class="form-group">
        <label for="name"
          >User ID:
          <input
            type="text"
            class="form-control"
            id="userid"
            name="userid"
            required
            minlength="4"
            pattern="[0-9]{4,5}"
            [(ngModel)]="user.id"
          />
        </label>
      </div>
      <button type="submit" [disabled]="!loginForm.form.valid">Submit</button>
    </form>
    <div [hidden]="!errorMessage" class="alert alert-danger">
      {{ serverMsg }}
    </div>
  `,
  styles: []
})
export class LoginComponent implements OnInit {
  user: User = new User();
  private serverMsg = "";

  constructor(private userService: UserService, public router: Router) {}

  ngOnInit(): void {}

  get errorMessage() {
    return this.serverMsg;
  }

  onSubmit() {
    this.userService.verifyUser(this.user).subscribe(
      user => {
        console.log(user);
        this.serverMsg = "";
        this.user = user;
        let navigationExtras: NavigationExtras = {
          queryParamsHandling: "preserve",
          preserveFragment: true,
          queryParams: { userId: user.id }
        };
        this.router.navigate(["/dashboard"], {queryParams: { userId: user.id }});
      },
      error => {
        console.error(error);
        this.serverMsg = error;
      }
    );
  }
}
