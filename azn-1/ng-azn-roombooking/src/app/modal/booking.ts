import { Room } from "./room";
import { User } from "./user";

export class Booking {
  bookingId: number;
  room: Room;
  startDateTime: Date;
  endDateTime: Date;
  user: User;
}
