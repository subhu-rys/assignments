export class Room {
  id: number;
  name: String;
  capacity: number;
  type: string;
}
