import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Booking } from "../modal/booking";
import { Room } from "../modal/room";
import { User } from "../modal/user";
import { BookingService } from "../service/booking.service";
@Component({
  selector: "app-room-booking",
  template: `
    <form (ngSubmit)="onSubmit()" #bookingForm="ngForm">
      <div class="form-group">
        <p>Booking for Room {{ room.name | titlecase }}</p>
        <label>
          Start Time:
          <input
            [owlDateTime]="dts"
            [owlDateTimeTrigger]="dts"
            [min]="startAt"
            placeholder="Start Date Time"
            id="dts"
            name="dts"
            required
            [(ngModel)]="booking.startDateTime"
          />
          <owl-date-time #dts></owl-date-time>
        </label>

        <label>
          End Time:
          <input
            [owlDateTime]="dte"
            [owlDateTimeTrigger]="dte"
            [min]="startAt"
            placeholder="End Date Time"
            id="dte"
            name="dte"
            required
            [(ngModel)]="booking.endDateTime"
          />
          <owl-date-time #dte></owl-date-time>
        </label>
      </div>
      <button type="submit" [disabled]="!bookingForm.form.valid">Submit</button>

    <div [hidden]="!errorMessage" class="alert alert-danger">
      {{ serverMsg }}
    </div>
    </form>
  `,
  styles: []
})
export class RoomBookingComponent implements OnInit {
  booking: Booking = new Booking();
  public startAt:Date = new Date();
  room: Room;
  private serverMsg = "";
  public userId: String;
  constructor(
    private bookingService: BookingService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data: { room: Room }) => {
      this.room = data.room;
    });

    this.route.queryParams.subscribe(params => {
        console.log(params);
        this.userId = params.userId;
      });

  }

  get errorMessage() {
    return this.serverMsg;
  }

  onSubmit() {
    this.booking.room = this.room;
    this.booking.user = new User();
    this.booking.user.id = Number(this.userId || 0);
    console.log(this.booking);
    if(this.userId && this.room){
    this.bookingService.saveBooking(this.booking).subscribe(
      bookingId => {
        console.log(bookingId);
        this.serverMsg = "";
        this.router.navigate(["/dashboard"], {queryParams: { userId: this.userId }});
      },
      error => {
        console.error(error);
        this.serverMsg = error;
      }
    );
    }
    else{
    this.router.navigate(["/logout"]);
    }
  }
}
