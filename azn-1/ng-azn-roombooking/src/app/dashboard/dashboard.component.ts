import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Booking } from "../modal/booking";
import { BookingService } from "../service/booking.service";

@Component({
  selector: "app-dashboard",
  template: `
    <nav>
      Welcome {{ userId }}
      <a routerLink="/dashboard" queryParamsHandling="preserve" routerLinkActive="active">Dashboard</a>
      <a routerLink="/booking" queryParamsHandling="preserve" routerLinkActive="active">Booking</a>
      <a routerLink="/logout" routerLinkActive="active">Logout</a>
    </nav>

    <br />

    <h2>Bookings</h2>
    <ul class="bookings">
      <li *ngFor="let booking of (bookings$ | async)">
        <a
          ><span class="badge">{{ booking.bookingId }}</span
          ><b>Room:</b> {{ booking.room.name | titlecase }} <b>Start Time:</b>
          {{ booking.startDateTime | date: "medium" }} <b>End Time:</b>
          {{ booking.endDateTime | date: "medium" }} <b>Booked by:</b>
          {{ booking.user.name | titlecase }}</a
        >
      </li>
    </ul>
  `,
  styles: [
    `
      .bookings {
        margin: 0 0 2em 0;
        list-style-type: none;
        padding: 0;
        width: 60em;
      }
      .bookings li {
        position: relative;
        cursor: pointer;
        background-color: #eee;
        margin: 0.5em;
        padding: 0.3em 0;
        height: 2em;
        border-radius: 4px;
      }

      .bookings li:hover {
        color: #607d8b;
        background-color: #ddd;
        left: 0.1em;
      }

      .bookings a {
        color: #888;
        text-decoration: none;
        position: relative;
        display: block;
      }

      .bookings a:hover {
        color: #607d8b;
      }

      .bookings .badge {
        display: inline-block;
        font-size: small;
        color: white;
        padding: 0.8em 0.7em 0 0.7em;
        background-color: #607d8b;
        line-height: 1em;
        position: relative;
        left: -1px;
        top: -4px;
        height: 2.1em;
        min-width: 16px;
        text-align: right;
        margin-right: 0.8em;
        border-radius: 4px 0 0 4px;
      }
    `
  ]
})
export class DashboardComponent implements OnInit {
  bookings$: Observable<Booking[]>;
  public userId: String;

  constructor(
    private bookingService: BookingService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
        console.log(params);
        this.userId = params.userId;
      });

    this.bookings$ = this.bookingService.fetchBooking();
  }
}
