node -v
10.15.3
npm -v
6.4.1
yarn -v
1.15.2
Angular CLI: 7.3.8
Node: 10.15.3
OS: linux x64
@angular-devkit/build-angular
@angular/cdk
ng-pick-datetime


####Spring Framework Documentation
* As of Spring Framework 5.1, Spring requires JDK 8+ (Java SE 8+) and provides out-of-the-box support for JDK 11 LTS.
* The Spring Framework is divided into modules. Applications can choose which modules they need. At the heart are the modules of the core container, including a configuration model and a dependency injection mechanism. Beyond that, the Spring Framework provides foundational support for different application architectures, including messaging, transactional data and persistence, and web. It also includes the Servlet-based Spring MVC web framework and, in parallel, the Spring WebFlux reactive web framework.
* As of Spring Framework 5.0, Spring requires the Java EE 7 level (e.g. Servlet 3.1+, JPA 2.1+) as a minimum
* Providing out-of-the-box integration with newer APIs at the Java EE 8 level (e.g. Servlet 4.0, JSON Binding API) when encountered at runtime. 
* Spring Framework 5, a major version upgrade, introduces a new non-blocking web framework called Spring WebFlux which uses Reactor to support the Reactive Streams API. It also offers a functional programming alternative to annotated controllers, first-class Kotlin language support, and first-class integration with JUnit 5.
* The Spring Boot 2.x line is built on Spring Framework 5.
* The Servlet-based Spring MVC remains included as a first-class choice alongside the new WebFlux framework. using Spring MVC if you are developing web apps that don't benefit from a non-blocking programming model, or that use blocking JPA or JDBC APIs for persistence (typically in combination with thread-bound transactions).
* The WebFlux framework in Spring Framework 5 uses Reactor as its async foundation. This project provides the core types, Mono to represent a single async value, and Flux to represent a stream of async values. 
* For handlers to be fully non-blocking, you need to use reactive libraries throughout the processing chain, all the way to the persistence layer. 
* Reactive Spring Data libraries are already available for Cassandra, MongoDB, Redis, and Couchbase. Please note that JPA and JDBC are inherently blocking APIs; we are still waiting for common ground around non-blocking relational database drivers.
* One suggestion for handling a mix of blocking and non-blocking code would be to use the power of a microservice boundary to separate the blocking backend datastore code from the non blocking front-end API. Alternatively, you may also go with a worker thread pool for blocking operations, keeping the main event loop non-blocking that way.
* Reactor uses the same underlying Pub/Sub API as RxJava and other libraries built on Reactive Streams so interoperability with other reactive libraries.
* Instead of taking ownership of the thread and and doing the work immediately, WebFlux controller methods operate on async Request and Response types.
* The WebFlux framework focuses on Tomcat and Jetty as well as Netty and Undertow
* The AsyncRestTemplate has been deprecated in favor of the new WebClient which provides a more fluent API, and is capable of both sync and async in one package. RestTemplate itself is not deprecated and there is nothing wrong with using it; the WebClient can be seen as its more modern successor
* Spring Framework 5 ships with automatic module name entries in the manifests of our Spring Framework 5 jars. The public API surface of the Spring libraries remains unchanged.

####Design Philosophy
* Spring lets you defer design decisions as late as possible. For example, you can switch persistence providers through configuration without changing your code. 
* Spring embraces flexibility and is not opinionated about how things should be done

####Meta-Annotations
* A meta-annotation is an annotation that is declared on another annotation.

####Stereotype Annotations
* A stereotype annotation is an annotation that is used to declare the role that a component plays within the application. For example, the @Repository annotation in the Spring Framework is a marker for any class that fulfills the role or stereotype of a repository (also known as Data Access Object or DAO).
* Any component annotated with @Component is a candidate for component scanning. Similarly, any component annotated with an annotation that is itself meta-annotated with @Component is also a candidate for component scanning. For example, @Service is meta-annotated with @Component. @Component, @Service, @Repository, @Controller, @RestController, and @Configuration. @Repository, @Service, etc. are specializations of @Component.

####Composed Annotations
* A composed annotation is an annotation that is meta-annotated with one or more annotations with the intent of combining the behavior associated with those meta-annotations into a single custom annotation.
* @TransactionalService is technically also a custom stereotype annotation that is meta-annotated with Spring's @Transactional and @Service annotations

####Attribute Aliases and Overrides
* An attribute alias is an alias from one annotation attribute to another annotation attribute. Attributes within a set of aliases can be used interchangeably and are treated as equivalent. 
* An attribute override is an annotation attribute that overrides (or shadows) an annotation attribute in a meta-annotation. 

####Spring Framework’s Inversion of Control (IoC) container.
* IoC is also known as dependency injection (DI).
* A process whereby objects define their dependencies (that is, the other objects they work with) only through constructor arguments, arguments to a factory method, or properties that are set on the object instance after it is constructed or returned from a factory method. The container then injects those dependencies when it creates the bean. This process is fundamentally the inverse (hence the name, Inversion of Control) of the bean itself controlling the instantiation or location of its dependencies by using direct construction of classes or a mechanism such as the Service Locator pattern.
* The org.springframework.beans and org.springframework.context packages are the basis for Spring Framework’s IoC container.
* The BeanFactory interface provides an advanced configuration mechanism capable of managing any type of object. ApplicationContext is a sub-interface of BeanFactory. ApplicationContext interface represents the Spring IoC container and is responsible for __instantiating, configuring, and assembling the beans__. 
* In Spring, the objects managed by the Spring IoC container are called beans.
* ClassPathXmlApplicationContext or FileSystemXmlApplicationContext
* Bean behavioral configuration elements, which state how the bean should behave in the container (scope, lifecycle callbacks, and so forth). 
* The bean definition
Property 					| Explained in
Class 						| Instantiating Beans
Name 						| Naming Beans
Scope 						| Bean Scopes
Constructor arguments 		| Dependency Injection
Properties 					| Dependency Injection
Autowiring mode 			| Autowiring Collaborators
Lazy initialization mode 	| Lazy-initialized Beans
Initialization method 		| Initialization Callbacks
Destruction method 			| Destruction Callbacks
```
//services.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- services -->
	<import resource="daos.xml"/>
    <bean id="petStore" class="org.springframework.samples.jpetstore.services.PetStoreServiceImpl">
        <property name="accountDao" ref="accountDao"/>
        <property name="itemDao" ref="itemDao"/>
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions for services go here -->

</beans>
```
```
// daos.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="accountDao"
        class="org.springframework.samples.jpetstore.dao.jpa.JpaAccountDao">
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <bean id="itemDao" class="org.springframework.samples.jpetstore.dao.jpa.JpaItemDao">
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions for data access objects go here -->

</beans>
```
```
// create and configure beans
ApplicationContext context = new ClassPathXmlApplicationContext("services.xml", "daos.xml");

// retrieve configured instance
PetStoreService service = context.getBean("petStore", PetStoreService.class);

// use configured instance
List<String> userList = service.getUsernameList(); 
```


I have replaced the @Autowired annotation with @Resource, which attempts a lookup by name before type, and added an explicit name to the child class. In order to disambiguate the parent from the child when attempting to autowire the parent, I have also added the @Primary annotation to the parent.


specify image format in separate column
always enum equivalent table 

!!!!!
QRCODE

####jsQR
- A pure javascript QR code reading library.
- This library takes in raw images and will locate, extract and parse any QR code found within.
```
npm install jsqr --save

// ES6 import
import jsQR from "jsqr";

const code = jsQR(imageData, width, height, options?);

if (code) {
  console.log("Found QR code", code);
}
```
- jsQR to scan a webcam stream you'll need to extract the ImageData from the video stream

####Arguments
imageData: Uint8ClampedArray [length of this array should be 4 * width * height]
width: width of the image
height: height of the image
options: inversionAttempts - (attemptBoth (default), dontInvert, onlyInvert, or invertFirst)

####Return Value
binaryData: Uint8ClampedArray
data: The string version of the QR code data.
location: key points of the QR code

####Consuming a webcam stream

####MediaDevices
- The MediaDevices.getUserMedia() method prompts the user for permission to use a media input which produces a MediaStream with tracks containing the requested types of media.
- It returns a Promise that resolves to a MediaStream object. If the user denies permission, or matching media is not available, then the promise is rejected with NotAllowedError or NotFoundError respectively.
- It's possible for the returned promise to neither resolve nor reject, as the user is not required to make a choice at all and may simply ignore the request with OverconstrainedError.
- AbortError, NotReadableError, SecurityError, TypeError [If a document isn't loaded in a secure context]
```
async function getMedia(pc) {
  let stream: MediaStream  = null;

  try {
    stream = await navigator.mediaDevices.getUserMedia(constraints: MediaStreamConstraints);
    /* use the stream */
  } catch(err) {
    /* handle the error */
  }
}
```
```
navigator.mediaDevices.getUserMedia(constraints: MediaStreamConstraints)
.then(function(stream: MediaStream ) {
  /* use the stream */
})
.catch(function(err) {
  /* handle the error */
});
```
```
navigator.mediaDevices.getUserMedia({ video: true, audio: false })
    .then(function(stream) {
        video.srcObject = stream;
        video.play();
    })
    .catch(function(err) {
        console.log("An error occurred: " + err);
    });
```
- If the current document isn't loaded securely, navigator.mediaDevices will be undefined, and you cannot use getUserMedia().
- A secure context is, in short, a page loaded using HTTPS or the file:/// URL scheme, or a page loaded from localhost.
- muted your camera (so-called "facemuting"), your camera's activity light goes out to indicate that the camera is not actively recording you
- A document loaded into a sandboxed <iframe> element cannot call getUserMedia() unless the <iframe> has its sandbox attribute set to allow-same-origin.
```
var front = false;
document.getElementById('flip-button').onclick = function() { front = !front; };

var constraints = { video: { facingMode: (front? "user" : "environment") } };
```

####Feature Policy 
- Examples of what you can do with Feature Policy:
  - Change the default behavior of autoplay on mobile and third party videos.
  - Restrict a site from using sensitive APIs like camera or microphone.
  - Allow iframes to use the fullscreen API.
  - Block the use of outdated APIs like synchronous XHR and document.write().
  - Ensure images are sized properly and are not too big for the viewport.

####WebRTC (Web Real-Time Communications)
- A technology which enables Web applications and sites to capture and optionally stream audio and/or video media, as well as to exchange arbitrary data between browsers without requiring an intermediary.
- WebRTC serves multiple purposes, and overlaps substantially with the Media Capture and Streams API. Together, they provide powerful multimedia capabilities to the Web, including support for audio and video conferencing, file exchange, identity management, and interfacing with legacy telephone systems by sending DTMF signals. 

navigator.mediaDevices.getSupportedConstraints()
```
let supported = navigator.mediaDevices.getSupportedConstraints();

document.getElementById("frameRateSlider").disabled = !supported["frameRate"];
```

proxy
sso
saml
oauth
network
protocol

```
[
    { "audio": true, "video": true },
    {
        "audio": true,
        "video": {
            "width": 1280,
            "height": 720
        }
    },
    {
        "audio": true,
        "video": {
            "width": { "min": 1280 },
            "height": { "min": 720 }
        }
    },
    {
        "audio": true,
        "video": {
            "width": {
                "min": 1024,
                "ideal": 1280,
                "max": 1920
            },
            "height": {
                "min": 776,
                "ideal": 720,
                "max": 1080
            }
        }
    },
    {
        "audio": true,
        "video": {
            "facingMode": "user"
        },
        "_comment": "prefer the front camera (if one is available) over the rear one"
    },
    {
        "audio": true,
        "video": {
            "facingMode": {
                "exact": "environment"
            }
        },
        "_comment": "rear camera"
    },

    {
        "video": {
            "deviceId": myPreferredCameraDeviceId
        },
        "_comment": "if you have a deviceId from mediaDevices.enumerateDevices(), you can use it to request a specific device against non-number deviceId constraint."
    },
    {
        "video": {
            "deviceId": {
                "exact": myExactCameraOrBustDeviceId
            }
        }
    },
    {
        "video": {
            "frameRate": {
                "ideal": 10,
                "max": 15
            }
        }
    },
    {
        "video": {
            "width": {
                "min": 640,
                "ideal": 1920
            },
            "height": {
                "min": 400,
                "ideal": 1080
            },
            "aspectRatio": {
                "ideal": 1.7777777778
            }
        },
        "audio": {
            "sampleSize": 16,
            "channelCount": 2
        }
    }
]
```
```
JSON.stringify(value: any, replacer?: (this:any, key: string, value: any) => any, space?: string | number)
```

####NGX-Bootstrap

####Video
- autoplay
- playsinline
- facingMode [ environment| ]
- controls
- loop
- muted
- canplay

```
<video id="leftVideo" playsinline controls loop muted>
	<source src="../../../video/chrome.webm" type="video/webm"/>
	<source src="../../../video/chrome.mp4" type="video/mp4"/>
	<p>This browser does not support the video element.</p>
</video>

<video id="rightVideo" playsinline autoplay></video>
```
```
const leftVideo = document.getElementById('leftVideo');
const rightVideo = document.getElementById('rightVideo');

leftVideo.addEventListener('canplay', () => {
  const stream = leftVideo.captureStream();
  rightVideo.srcObject = stream;
});
```
```
const canvas = document.querySelector('canvas');
const video = document.querySelector('video');

const stream = canvas.captureStream();
video.srcObject = stream;
```
```
const video = document.querySelector('video');
const canvas = window.canvas = document.querySelector('canvas');
const button = document.querySelector('button');
button.onclick = function() {
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  
  canvas.setAttribute('width', width);
  canvas.setAttribute('height', height);
		
  canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
};

filterSelect.onchange = function() {
  video.className = filterSelect.value;
};

const constraints = {
  audio: false,
  video: true
};

function handleSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  video.srcObject = stream;
}

function handleError(error) {
  console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);

clearphoto() {
    var context = canvas.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);
    var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
}
```
```
    stream.getTracks().forEach(track => {
      track.stop();
    });
```

- getUserMedia(): capture audio and video.
- MediaRecorder: record audio and video.
- RTCPeerConnection: stream audio and video between users.
- RTCDataChannel: stream data between users.

- Client applications need to traverse NAT gateways and firewalls, and peer to peer networking needs fallbacks in case direct connection fails. As part of this process, the WebRTC APIs use STUN servers to get the IP address of your computer, and TURN servers to function as relay servers in case peer-to-peer communication fails.
- With SCTP, the protocol used by WebRTC data channels, reliable and ordered data delivery is on by default. 
- WebRTC stats and debug data are available from chrome://webrtc-internals.
- test.webrtc.org can be used to check your local environment and test your camera and microphone.

####Best practice
- Make sure your video element doesn't overflow its container. We've added width and max-width to set a preferred size and a maximum size for the video. The browser will calculate the height automatically:
```
video {
  max-width: 100%;
  width: 320px;
}
```
- Don't forget the autoplay attribute on the video element. Without that, you'll only see a single frame!
- The MediaStreamTrack is actively using the camera, which takes resources and keeps the camera open (and camera light on). When you are no longer using a track make sure to call track.stop() so that the camera can be closed.


@angular/cli@8.0.3

Webcam Capture Live Streaming

npm install jsqr --save

`````````````````````````````````````````````
EncodeHintType.ERROR_CORRECTION
ErrorCorrectionLevel.L

BitMatrix MultiFormatWriter::new::encode(String contents, BarcodeFormat format, int width, int height, Map<EncodeHintType,?> hints) 
BitMatrix QRCodeWriter::new::encode(String contents, BarcodeFormat format, int width, int height, Map<EncodeHintType,?> hints) 

MatrixToImageWriter::writeToPath(BitMatrix matrix, String format, Path file)
MatrixToImageWriter::writeToStream(BitMatrix matrix, String format, OutputStream stream)
MatrixToImageWriter::toBufferedImage(BitMatrix matrix, MatrixToImageConfig config) :BufferedImage <- MatrixToImageConfig::new(int onColor, int offColor) 

ImageIO::write(RenderedImage im, String formatName, File output)

BufferedImage(int width, int height, int imageType)::createGraphics() :Graphics2D
Graphics2D::fillRect(int x, int y, int width, int height)
SVGGraphics2D(int width, int height)::drawImage(java.awt.Image img, int x, int y, int w, int h, java.awt.image.ImageObserver observer)
SVGGraphics2D(int width, int height)::setComposite

Files.copy

MultiFormatReader::decode(BinaryBitmap image) :Result::getText
BinaryBitmap(Binarizer binarizer) 
Binarizer(LuminanceSource source) 
BufferedImageLuminanceSource(BufferedImage image) 

ImageIO::read(File input) :BufferedImage
`````````````````````````````````````````````


        const wait = (ms) => new Promise(res => setTimeout(res, ms));
        const startAsync = async callback => {
            await wait(1000);
            callback('Hello');
        };
        startAsync(text => console.log(text));
		
!!!!!

####Column Definitions:
- Each column in the grid is defined using a column definition. Columns are positioned in the grid according to the order the ColDef's are specified in the grid options.
```javascript
var gridOptions = {
    // define 3 columns
    columnDefs: [
        {headerName: 'Athlete', field: 'athlete'},
        {headerName: 'Sport', field: 'sport'},
        {headerName: 'Age', field: 'age'}
    ],

    // other grid options here...
}
```
```javascript
var gridOptions = {
    columnDefs: [
        // put the three columns into a group
        {headerName: 'Group A',
            children: [
                {headerName: 'Athlete', field: 'athlete'},
                {headerName: 'Sport', field: 'sport'},
                {headerName: 'Age', field: 'age'}
            ]
        }
    ],

    // other grid options here...
}
```
- Grid provides additional ways to help simplify and avoid duplication of column definitions.
- `defaultColDef`: contains column properties all columns will inherit.
- `defaultColGroupDef`: contains column group properties all column groups will inherit.
- `columnTypes`: specific column types containing properties that column definitions can inherit.
- Default columns and column types can specify any of the column properties available on a column.
```
var gridOptions = {
    rowData: myRowData,

    // define columns
    columnDefs: [
        // uses the default column properties
        {headerName: 'Col A', field: 'a'},

        // overrides the default with a number filter
        {headerName: 'Col B', field: 'b', filter: 'agNumberColumnFilter'},

        // overrides the default using a column type
        {headerName: 'Col C', field: 'c', type: 'nonEditableColumn'},

        // overrides the default using a multiple column types
        {headerName: 'Col D', field: 'd', type: ['dateColumn', 'nonEditableColumn']}
    ],

    // a default column definition with properties that get applied to every column
    defaultColDef: {
        // set every column width
        width: 100,
        // make every column editable
        editable: true,
        // make every column use 'text' filter by default
        filter: 'agTextColumnFilter'
    },

    // if we had column groups, we could provide default group items here
    defaultColGroupDef: {}

    // define a column type (you can define as many as you like)
    columnTypes: {
        "nonEditableColumn": {editable: false},
        "dateColumn": {filter: 'agDateColumnFilter', filterParams: {comparator: myDateComparator}, suppressMenu:true}
        }
    }

    // other grid options here...
}
```
- When the grid creates a column it starts with the default column, then adds in anything from the column type, then finally adds in items from the column definition.

####Provided Column Types:
Numeric Columns - numericColumn
It is possible to save and subsequently restore the column state via the Column API. Examples of state include column visibility, width, row groups and values.
columnApi.getColumnState(): Returns the state of a particular column.
columnApi.setColumnState(state): To set the state of a particular column. The column state used by the above methods is an array of objects that mimic the colDef's which can be converted to and from JSON.
```
[
  {colId: "athlete", aggFunc: "sum",  hide: false, rowGroupIndex: 0,    width: 150, pinned: null},
  {colId: "age",     aggFunc: null,   hide: true,  rowGroupIndex: null, width: 90,  pinned: 'left'}
]
```
colId: The ID of the column. 
aggFunc: If this column is a value column, this field specifies the aggregation function. If the column is not a value column, this field is null.
hide: True if the column is hidden, otherwise false.
rowGroupIndex: The index of the row group. If the column is not grouped, this field is null. If multiple columns are used to group, this index provides the order of the grouping.
width: The width of the column. If the column was resized, this reflects the new value.
pinned: The pinned state of the column. Can be either 'left' or 'right'

!!!!!

AGRID
####Column Definitions:
- Each column in the grid is defined using a column definition. Columns are positioned in the grid according to the order the ColDef's are specified in the grid options.
```javascript
var gridOptions = {
    // define 3 columns
    columnDefs: [
        {headerName: 'Athlete', field: 'athlete'},
        {headerName: 'Sport', field: 'sport'},
        {headerName: 'Age', field: 'age'}
    ],

    // other grid options here...
}
```
```javascript
var gridOptions = {
    columnDefs: [
        // put the three columns into a group
        {headerName: 'Group A',
            children: [
                {headerName: 'Athlete', field: 'athlete'},
                {headerName: 'Sport', field: 'sport'},
                {headerName: 'Age', field: 'age'}
            ]
        }
    ],

    // other grid options here...
}
```
- Grid provides additional ways to help simplify and avoid duplication of column definitions.
- `defaultColDef`: contains column properties all columns will inherit.
- `defaultColGroupDef`: contains column group properties all column groups will inherit.
- `columnTypes`: specific column types containing properties that column definitions can inherit.
- Default columns and column types can specify any of the column properties available on a column.
```
var gridOptions = {
    rowData: myRowData,

    // define columns
    columnDefs: [
        // uses the default column properties
        {headerName: 'Col A', field: 'a'},

        // overrides the default with a number filter
        {headerName: 'Col B', field: 'b', filter: 'agNumberColumnFilter'},

        // overrides the default using a column type
        {headerName: 'Col C', field: 'c', type: 'nonEditableColumn'},

        // overrides the default using a multiple column types
        {headerName: 'Col D', field: 'd', type: ['dateColumn', 'nonEditableColumn']}
    ],

    // a default column definition with properties that get applied to every column
    defaultColDef: {
        // set every column width
        width: 100,
        // make every column editable
        editable: true,
        // make every column use 'text' filter by default
        filter: 'agTextColumnFilter'
    },

    // if we had column groups, we could provide default group items here
    defaultColGroupDef: {}

    // define a column type (you can define as many as you like)
    columnTypes: {
        "nonEditableColumn": {editable: false},
        "dateColumn": {filter: 'agDateColumnFilter', filterParams: {comparator: myDateComparator}, suppressMenu:true}
        }
    }

    // other grid options here...
}
```
- When the grid creates a column it starts with the default column, then adds in anything from the column type, then finally adds in items from the column definition.

####Provided Column Types:
Numeric Columns - numericColumn
It is possible to save and subsequently restore the column state via the Column API. Examples of state include column visibility, width, row groups and values.
columnApi.getColumnState(): Returns the state of a particular column.
columnApi.setColumnState(state): To set the state of a particular column. The column state used by the above methods is an array of objects that mimic the colDef's which can be converted to and from JSON.
```
[
  {colId: "athlete", aggFunc: "sum",  hide: false, rowGroupIndex: 0,    width: 150, pinned: null},
  {colId: "age",     aggFunc: null,   hide: true,  rowGroupIndex: null, width: 90,  pinned: 'left'}
]
```
colId: The ID of the column. 
aggFunc: If this column is a value column, this field specifies the aggregation function. If the column is not a value column, this field is null.
hide: True if the column is hidden, otherwise false.
rowGroupIndex: The index of the row group. If the column is not grouped, this field is null. If multiple columns are used to group, this index provides the order of the grouping.
width: The width of the column. If the column was resized, this reflects the new value.
pinned: The pinned state of the column. Can be either 'left' or 'right'

!!!!
ANGULAR
npm uninstall -g @angular/cli
npm cache verify
# if npm version is < 5 then use `npm cache clean` 
npm install -g @angular/cli@6.1.3
npm install -g @angular/cli@latest

nodejs node_modules/node-sass/scripts/install.js
npm rebuild node-sass --force
npm update
ng serve --host 0.0.0.0 --port 4201
npm install --python=python2.7
npm config set python python2.7
npm install node-sass --save-dev
npm list
npm list --depth=0
npm list -g

ng new codetub-ui --createApplication=false --skipInstall=true --dryRun=true
ng config --global=false cli.packageManager npm
npm config set registry https://artifactory.xyz.net/npm_
ng generate application xyz-bootstrap --style=scss --routing --skipInstall=true --dryRun=true
npm cache clean --force
npm install

yarn config get registry
yarn config set registry=http://registry.npm.personal/

ng build xyz-bootstrap --verbose=true
ng serve xyz-bootstrap --host=localhost --port 4202 --liveReload=true --watch=true --open=true --verbose=true
ng serve --proxy-config proxy.conf.json

netstat -ano | findstr :4200
taskkill /PID 1380 /F

$ yarn add bootstrap
yarn add v1.16.0
info No lockfile found.
[1/4] Resolving packages...
[2/4] Fetching packages...
info There appears to be trouble with your network connection. Retrying...
info There appears to be trouble with your network connection. Retrying...
info There appears to be trouble with your network connection. Retrying...
info There appears to be trouble with your network connection. Retrying...
info There appears to be trouble with your network connection. Retrying...
info There appears to be trouble with your network connection. Retrying...
error An unexpected error occurred: "https://registry.yarnpkg.com/@angular/common/-/common-8.0.2.tgz: ESOCKETTIMEDOUT".
info If you think this is a bug, please open a bug report with the information provided in "C:\\code-base\\codetub-ui\\yarn-error.log".
info Visit https://yarnpkg.com/en/docs/cli/add for documentation about this command.


$ npm install

> core-js@2.6.9 postinstall C:\code-base\codetub-ui\node_modules\babel-runtime\node_modules\core-js
> node scripts/postinstall || echo "ignore"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js on Open Collective or Patreon:
> https://opencollective.com/core-js
> https://www.patreon.com/zloirock

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)


> core-js@2.6.9 postinstall C:\code-base\codetub-ui\node_modules\karma\node_modules\core-js
> node scripts/postinstall || echo "ignore"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js on Open Collective or Patreon:
> https://opencollective.com/core-js
> https://www.patreon.com/zloirock

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)


> @angular/cli@8.0.4 postinstall C:\code-base\codetub-ui\node_modules\@angular\cli
> node ./bin/postinstall/script.js

white-space: nowrap;
transition: background-color 0.2s;
visibility: hidden;
.Demo::after
margin-bottom: var(--space);
background: hsla(31, 15%, 50%, 0.1);
overflow-x: hidden;
 list-style: none;
.Grid--flexCells > .Grid-cell 
.container::-webkit-scrollbar

    overflow-y: scroll;
    scrollbar-width: none; /* Firefox */
    -ms-overflow-style: none;  /* IE 10+ */
	
display: flex;
  flex-wrap: wrap;
   flex: 1;
     align-items: flex-start;
	 align-items: flex-end;
	 align-items: center;
The viewport-percentage lengths are relative to the size of the initial containing block. 
vw (% of the viewport width)
vh (% of the viewport height)
vi (1% of the viewport size in the direction of the root element's inline axis)
vb (1% of the viewport size in the direction of the root element's block axis)
vmin (the smaller of vw or vh)
vmax (the larger or vw or vh)

unit	name	equivalence
cm	centimeters	1cm = 96px/2.54
mm	millimeters	1mm = 1/10th of 1cm
Q	quarter-millimeters	1Q = 1/40th of 1cm
in	inches	1in = 2.54cm = 96px
pc	picas	1pc = 1/6th of 1in
pt	points	1pt = 1/72th of 1in
px	pixels	1px = 1/96th of 1in

CCard:
reward points value & validity
foreign currency markup of 2%
Convenience fee waiver of 1% on all fuel transactions
an extremely low interest charge of just 3.49% per month

rxjs:
Observables - Observables are lazy collections of multiple values over time. You could think of lazy observables as newsletters. For each subscriber a new newsletter is created. Now if you keep that subscription to the newsletter open, you will get a new one every once and a while. The sender decides when you get it but all you have to do is just wait until it comes straight into your inbox.

Promises - a key difference as promises always return only one value. Another thing is that observables are cancelable.

Network:

NODE_ENV specifically is used (by convention) to state whether a particular environment is a production or a development environment. A common use-case is running additional debugging or logging code if running in a development environment.


netsh winhttp show proxy 
ipconfig

chrome://net-internals/#proxy

chrome://net-internals/#http2

curl http://www.google.co.in --proxy 165.225.106.34:10263
curl --verbose -x http://165.225.106.34:10263 -L http://www.google.co.in

--proxy-user username:password 

npm config set proxy "http://domain%5Cusername:password@servername:port/"
npm config get strict-ssl/registry

npm cache verify
npm cache clean --force

nodeSassConfig {
  binarySite: 'https://github.com/sass/node-sass/releases/download'
}

npm install node-sass@4.11.0 --save-dev --save-exact --sass-binary-site=https://artifactory.xyz.net/npm_/node-sass/-
npm install node-sass@4.11.0 --save-dev --save-exact --sass-binary-path=C:\Users\xyz\Documents\lib\win32-x64-64_binding.node

registry=https://artifactory.xys.net/api/npm/npm
sass_binary_path=C:\Users\xyz\Documents\lib\win32-x64-64_binding.node


SAML is based on browser redirects
OpenID Connect / OAuth
SAML is XML with lots of assertions (claims) and OAuth is JSON with essentially a canned set of attributes
Postman requires a JWT token


SAML is an XML-based open-standard for web-based single sign-on.
SAML is not yet supported for Postman.
Since there's no password for SAML logins, there's no risk of credentials being stolen or used by unauthorized users.


https://ping.xyz.com/as/authorization.oauth2?client_id=-sf&redirect_uri=http://localhost:8080/login&response_type=code&scope=edit%20openid%20profile%20email&state=6Qc2gf
https://ping.xyz.com/as/authorization.oauth2?client_id=-sf&redirect_uri=http://localhost:8080/login&response_type=code&scope=edit%20openid%20profile%20email&state=7KsWwM

https://ping.xyz.com/as/XmSvL/resume/as/authorization.ping
Set-Cookie: JSESSIONID=15b2dkzd13b079re70awsescc;Path=/;HttpOnly
Set-Cookie: PF=NW1omXPlOzIxq6ivrnrx14nxRc3Vn58kM0vyyYgA4f8o;Path=/;Secure;HttpOnly
Set-Cookie: BIGipServerPool_pingapac_HTTPS=2390766858.20480.0000; path=/

http://localhost:8080/login?code=StjfX_O--BRRDlecoBQOuzszC-T7SaGrzX0AAAEu&state=7KsWwM

Set-Cookie: JSESSIONID=B23A0CBAB845A464E4B91EA016712D54;path=/;HttpOnly

controlled delivery model

javatuples
QueryByExampleExecutor, JpaSpecificationExecutor
@SqlResultSetMapping
@MappedSuperclass

@HostBinding() and @HostListener() 

Volatile state, use ActivatedRoute.
Static state, use ActivatedRouteSnapshot.

Query
TypedQuery
NamedQuery
NavtiveQuery
JPQL queries 
Criteria API Query

getEntityManager().createQuery
getEntityManager().createNamedQuery
getEntityManager().createNativeQuery
getEntityManager().createQuery

@NamedNativeQuery
@Query(nativeQuery=true)
projections
union stack horizontal vertical
Custom Types in Hibernate AbstractSingleColumnStandardBasicType, UserType, CompositeUserType, Basic Type Registry

Named queries
@Query
With JPA 2.0 it is possible to define type-safe queries using the CriteriaQuery API
an open-source project “QueryDSL” that also allows us to specify type-safe queries, 
QueryDSL + Spring Data JPA

JPA EntityManager
JPA named queries
Spring Data JPA

LocalDate today = new LocalDate();

CriteriaBuilder builder = em.getCriteriaBuilder();
CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
Root<Customer> root = query.from(Customer.class);

Predicate hasBirthday = builder.equal(root.get(Customer_.birthday), today);
Predicate isLongTermCustomer = builder.lessThan(root.get(Customer_.createdAt), today.minusYears(2);
query.where(builder.and(hasBirthday, isLongTermCustomer));
em.createQuery(query.select(root)).getResultList();

~~~~~~~~~~~~~~~~~~~

LocalDate today = new LocalDate();

QCustomer customer = QCustomer.customer;
BooleanExpression hasBirthday = customer.birthday.eq(today);
BooleanExpression isLongTermCustomer = customer.createdAt.lt(today.minusYears(2));
new JPAQuery(em)
 .from(customer)
 .where(hasBirthday.and(isLongTermCustomer))
 .list(customer);
 
~~~~~~~~~~~~~~~~~~~~~~`

class CustomerExpressions {
 public static BooleanExpression hasBirthday() {
  LocalDate today = new LocalDate();
  return QCustomer.customer.birthday.eq(today);
 }
 public static BooleanExpression isLongTermCustomer() {
  LocalDate today = new LocalDate();
  return QCustomer.customer.createdAt.lt(today.minusYears(2));
 }
}

```````````````````

hasBirthday().and(isLongTermCustomer())

``````````````````````````
public void sendGifts() {
 Predicate shouldRecieveGiftToday = hasBirthday().and(isLongTermCustomer());
 for(Customer customer: customerRepository.findAll(shouldRecieveGiftToday)) {
  giftSender.sendGiftTo(customer);
 }
}

```````````````````````````````

public class CustomerRepositoryImpl
 extends QueryDslRepositorySupport
 implements CustomerRepositoryCustom {

 public Iterable<Customer> findAllLongtermCustomersWithBirthday() {
  QCustomer customer = QCustomer.customer;
  return from(customer)
   .where(hasBirthday().and(isLongTermCustomer()))
   .list(customer);
 }

}

~~~~~~~~~~
@Projection(name = "customBook", types = { Book.class }) 
public interface CustomBook {
  
    @Value("#{target.id}")
    long getId(); 
     
    String getTitle();
         
    @Value("#{target.getAuthors().size()}")
    int getAuthorCount();
}

As a general rule, we can access a projection’s result at http://localhost:8080/books/1?projection={projection name}.

Also, note that we need to define our Projection in the same package as our models. 

Excerpts are projections which we apply as default views to resource collections.
@RepositoryRestResource(excerptProjection = CustomBook.class)
public interface BookRepository extends CrudRepository<Book, Long> {}
~~~~~~~~~~~~~~~~~~~~~~~~~~~

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity

netstat -ano | findstr :4200
taskkill /PID 13292 /F

5YZ59PA

mvn clean install -Dmaven.test.skip=true -rf :tool-model

//            

git --git-dir /c/code-base/tool/.git diff > "/c/Users/xyzz/Sync/x.patch"

materialzed views

constructor(private rd: Renderer2) {}
void 0’

hp 5032287

1850 × 2700 6.1 x 8.9
2100 × 1900 6.9 x 6.3

1550 × 1740 5.1 x 5.8

MRL

1965 × 1860 6.5 x 6.2
1865 × 1860 6.2 x 6.2
1715 × 1982 5.7 x 6.6
1650 × 1982 5.5 x 6.6

S

1550 × 1650 5.1 x 5.5

S-4

1350 x 1550 4.5 x 5.1

!!!! J

1500 x 2000 5 x 6.6
1550 x 1250 

!!!! C

1800 x 1600 6 x 5.3
1700 x 1800 5.6 x 6
1800 x 1500 6 x 5
1600 x 1800 5.3 x 6

1550 x 1525
1650 x 1475

Jse(@ElementCollection Map<String, Section> sections;)
	Section(@ElementCollection Map<String, String> content; List<JseField> jseFields)
		JseField(@ElementCollection Map<String, String> content;)
		
JSE

JSE_SECTIONS
SECTION
SECTION_CONTENT

SECTION_JSE_FIELDS
JSE_FIELD
JSE_FIELD_CONTENT

kdrg040 
ksgq245

com.oracle:ojdbc7
com.fasterxml.jackson.dataformat:jackson-dataformat-xml
com.jcraft:jsch
com.zaxxer:HikariCP
org.springframework.retry:spring-retry

webdriver.chrome.driver

plugin:
org.jvnet.jaxb2.maven2:maven-jaxb2-plugin
com.lazerycode.jmeter:jmeter-maven-plugin
maven-resources-plugin

@ConfigurationProperties(prefix = "spring.datasource")

Received fatal alert: handshake_failure; nested exception is javax.net.ssl.SSLHandshakeException: Received fatal alert: handshake_failure

sonar:sonar -Dsonar.host.url=https://sonarqube.a.net -DproxySet=true -Dhttp.proxyHost=SE.a.net -Dhttp.proxyPort=94xx


https://www.tutorialspoint.com/javamail_api/javamail_api_send_inlineimage_in_email.htm


Digitalization 

.htaccess file - stands for “hypertext access” (redirects, enabling compression, rewriting urls, leveraging browser caching)

parts of the UI will not start rendering until the render-blocking requests have finished loading
The rel="preload" attribute value can be applied to several file formats, including CSS, JS, fonts, images and more. You should set the corresponding as attribute depending on the type of file. 
<link  rel="preload" as="style" href='https://fonts.googleapis.com/css?family=Roboto:100,900|Material+Icons'>
Using preload or prefetch only fetches the resource, but it doesn’t apply it. Instead, it keeps it in memory and is up to you to decide when to load it.
<link rel="preload" as="style" onload="this.rel = 'stylesheet'" href='https://fonts.googleapis.com/css?family=Roboto:100,900|Material+Icons'>
add a <noscript> tag as a fallback when JavaScript is disabled or fails to load
<noscript>
  <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Roboto:400,600|Material+Icons'>
</noscript>
Preloading and prefetching gives us more power on how the resources are loaded, boosting web performance and allowing for faster Progressive Web Apps. 