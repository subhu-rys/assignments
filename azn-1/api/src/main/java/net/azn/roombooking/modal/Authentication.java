package net.azn.roombooking.modal;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Authentication{
    Long id;
}