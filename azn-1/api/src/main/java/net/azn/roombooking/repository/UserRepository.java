package net.azn.roombooking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.azn.roombooking.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}