package net.azn.roombooking.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import net.azn.roombooking.entity.Booking;
import net.azn.roombooking.entity.Room;
import net.azn.roombooking.entity.User;
import net.azn.roombooking.modal.Authentication;
import net.azn.roombooking.repository.BookingRepository;
import net.azn.roombooking.repository.RoomRepository;
import net.azn.roombooking.repository.UserRepository;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RoomBookingService {

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@RequestBody Authentication loginForm) {
        Optional<User> user = userRepository.findById(loginForm.getId());
        if (user.isPresent()) {
            return new ResponseEntity<User>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("User not authorized", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserInfo(@PathVariable("id") Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return new ResponseEntity<User>(user.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/room")
    public List<Room> getRoom() {
        return roomRepository.findAll();
    }

    @GetMapping("/room/{id}")
    public ResponseEntity<?> getRoom(@PathVariable("id") Long id) {
        Optional<Room> room = roomRepository.findById(id);
        if (room.isPresent()) {
            return new ResponseEntity<Room>(room.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Room not listed", HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/bookings")
    public List<Booking> getRoomBookings() {
        return (List<Booking>) bookingRepository.findAll();
    }

    @PostMapping("/booking")
    public ResponseEntity<?> bookRoom(@RequestBody Booking booking) {
        // TODO: user trying to book a room by overlapping two days
        // TODO: User trying to book a room by overlapping with another booking.
        // TODO: starttime greater than locale now time and end time greater than start time.
        Optional<List<Booking>> oBookings = bookingRepository.findAllByRoomAndStartDateTimeOrEndDateTimeBetweenQuery(
                booking.getRoom(), booking.getStartDateTime(), booking.getEndDateTime());
        if (oBookings.isPresent()) {
            return new ResponseEntity<List<Booking>>(oBookings.get(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Long>(bookingRepository.save(booking).getBookingId(), HttpStatus.OK);
    }

}
