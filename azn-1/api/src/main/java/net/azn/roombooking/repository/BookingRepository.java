package net.azn.roombooking.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.azn.roombooking.entity.Booking;
import net.azn.roombooking.entity.Room;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    @Query("select b from Booking b where b.room=:room and ((b.startDateTime >= :start and b.startDateTime < :end ) or (b.endDateTime > :start and b.endDateTime <= :end ) or (b.startDateTime <= :start and b.endDateTime >= :end ))")
    Optional<List<Booking>> findAllByRoomAndStartDateTimeOrEndDateTimeBetweenQuery(Room room, LocalDateTime start,
            LocalDateTime end);
}
