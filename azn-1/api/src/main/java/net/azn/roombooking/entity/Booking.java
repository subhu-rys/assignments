package net.azn.roombooking.entity;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Getter
@NoArgsConstructor
@ToString
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "booking_seq")
    @SequenceGenerator(name = "booking_seq", initialValue = 101)
    private Long bookingId;
    @OneToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "room_id")
    private Room room;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    @OneToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    public Booking(Room room, LocalDateTime startDateTime, LocalDateTime endDateTime, User user) {
        this.room = room;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.user = user;
    }
}
