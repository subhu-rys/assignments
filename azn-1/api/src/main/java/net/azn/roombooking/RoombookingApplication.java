package net.azn.roombooking;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import net.azn.roombooking.entity.Booking;
import net.azn.roombooking.entity.Room;
import net.azn.roombooking.entity.User;
import net.azn.roombooking.repository.BookingRepository;
import net.azn.roombooking.repository.RoomRepository;
import net.azn.roombooking.repository.UserRepository;
import net.azn.roombooking.service.RoomBookingService;

@SpringBootApplication
public class RoombookingApplication {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    RoomBookingService roomBookingService;

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(RoombookingApplication.class, args);
        for (String name : applicationContext.getBeanDefinitionNames()) {
            System.out.println(name);
        }
    }

    @Bean
    CommandLineRunner init(UserRepository userRepository) {
        return args -> {
            List<User> users = new ArrayList<>();
            User litlo = new User("litlo");
            User bridgo = new User("bridgo");
            users.add(litlo);
            users.add(bridgo);
            users.stream().forEach(user -> {
                userRepository.save(user);
            });
            userRepository.findAll().forEach(System.out::println);

            List<Room> rooms = new ArrayList<>();
            Room littleWood = new Room("little wood", 10, Room.Type.HALL);
            Room cambridge = new Room("cambridge", 15, Room.Type.CONFERENCE);
            rooms.add(littleWood);
            rooms.add(cambridge);
            rooms.stream().forEach(room -> {
                roomRepository.save(room);
            });
            roomRepository.findAll().forEach(System.out::println);

            List<Booking> bookings = new ArrayList<>();
            bookings.add(new Booking(littleWood, LocalDateTime.now().plus(Duration.ofHours(5)),
                    LocalDateTime.now().plus(Duration.ofHours(10)), litlo));
            bookings.add(new Booking(cambridge, LocalDateTime.now().plus(Duration.ofHours(8)),
                    LocalDateTime.now().plus(Duration.ofHours(12)), bridgo));
            bookings.stream().forEach(booking -> {
                bookingRepository.save(booking);
            });
            bookingRepository.findAll().forEach(System.out::println);

            Optional<List<Booking>> bookingx = bookingRepository.findAllByRoomAndStartDateTimeOrEndDateTimeBetweenQuery(
                    littleWood, LocalDateTime.now(), LocalDateTime.now().plus(Duration.ofHours(4)));
            bookingx.ifPresent(System.out::println);

            bookingx = bookingRepository.findAllByRoomAndStartDateTimeOrEndDateTimeBetweenQuery(littleWood,
                    LocalDateTime.now(), LocalDateTime.now().plus(Duration.ofHours(7)));
            bookingx.ifPresent(System.out::println);

            Booking newBooking = new Booking(cambridge, LocalDateTime.now().plus(Duration.ofHours(8)),
                    LocalDateTime.now().plus(Duration.ofHours(12)), bridgo);
            System.out.println(roomBookingService.bookRoom(newBooking));

            newBooking = new Booking(cambridge, LocalDateTime.now().plus(Duration.ofDays(2)),
                    LocalDateTime.now().plus(Duration.ofDays(2)).plus(Duration.ofHours(2)), bridgo);
            System.out.println(roomBookingService.bookRoom(newBooking));

        };
    }
}
