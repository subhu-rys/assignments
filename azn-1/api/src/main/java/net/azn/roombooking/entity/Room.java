package net.azn.roombooking.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Getter
@NoArgsConstructor
@ToString
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "room_seq")
    @SequenceGenerator(name = "room_seq", initialValue = 1)
    private Long id;
    private String name;
    private long capacity;
    private Type type;
    // @OneToMany(fetch = FetchType.LAZY, mappedBy = "room")
    // private List<Booking> bookingList;

    public Room(String name, long capacity, Type type) {
        this.name = name;
        this.capacity = capacity;
        this.type = type;
    }

    public enum Type {
        HALL, CONFERENCE;
    }
}
